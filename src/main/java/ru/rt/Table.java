package ru.rt;

public interface Table<K,V> {
    V put(K key, V val);
    V get(K key);
    V delete(K key);
}

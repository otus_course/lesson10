package ru.rt;

public class Main {
    public static final int size = 1_000_000;

    public static void main(String[] args) throws Exception {
        HashTable<String, String> table = new HashTable<>();
        for (int i = 0; i < size; i++) {
            table.put(String.valueOf(i),String.valueOf(i));
        }

        assert table.size() == size : "Что-то пошло не так";

        for (int i = 0; i < size; i++) {
            String val = table.get(String.valueOf(i));

            assert val != null && val.equals(String.valueOf(i));
        }

        for (int i = 0; i < size; i++) {
            assert table.delete(String.valueOf(i)) != null : "Must not be null!";
        }

        assert table.size() == 0 : "Что-то пошло не так";
    }

}

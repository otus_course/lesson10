package ru.rt;

public class HashTable<K,V> implements Table<K,V>{
    private Entry<K, V>[] table;
    private transient int count;
    private int threshold;
    private double loadFactor = 0.75;
    private int defaultLength = 50;

    public HashTable(){
        this.table = new Entry[defaultLength];
        this.threshold = (int)(defaultLength * loadFactor);
        this.count = 0;
    }

    public int size(){
        return count;
    }

    @Override
    public V put(K key, V val) {
        if (val == null) {
            throw new NullPointerException();
        } else {
            Entry<K, V>[] tab = table;
            int hash = key.hashCode();
            int index = hash % tab.length;

            for(Entry entry = tab[index]; entry != null; entry = entry.next) {
                if (entry.hash == hash && entry.key.equals(key)) {
                    V old = (V)entry.value;
                    entry.value = val;
                    return old;
                }
            }

            addEntry(hash, key, val, index);
            return null;
        }
    }

    private void addEntry(int hash, K key, V value, int index) {
        Entry<K, V>[] tab = this.table;
        if (count >= threshold) {
            rehash();
            tab = this.table;
            hash = key.hashCode();
            index = hash % tab.length;
        }

        Entry<K, V> e = tab[index];
        tab[index] = new Entry(hash, key, value, e);
        this.count++;
    }

    private void rehash() {
        Entry<K, V>[] oldMap = this.table;
        int oldCapacity = oldMap.length;

        int newCapacity = oldCapacity * 2 + 1;


        Entry<K, V>[] newMap = new Entry[newCapacity];
        threshold = (int)(newCapacity * loadFactor);
        table = newMap;
        int i = oldCapacity;

        Entry e;
        int index;
        while(i-- > 0) {
            for(Entry old = oldMap[i]; old != null; newMap[index] = e) {
                e = old;
                old = old.next;
                index = e.hash % newCapacity;
                e.next = newMap[index];
            }
        }
    }

    @Override
    public V get(K key) {
        Entry<K, V>[] tab = this.table;
        int hash = key.hashCode();
        int index = hash % tab.length;

        for(Entry e = tab[index]; e != null; e = e.next) {
            if (e.hash == hash && e.key.equals(key)) {
                return (V)e.value;
            }
        }

        return null;
    }

    @Override
    public V delete(K key) {
        Entry<K, V>[] tab = this.table;
        int hash = key.hashCode();
        int index = hash % tab.length;
        Entry<K, V> e = tab[index];

        for(Entry prev = null; e != null; e = e.next) {
            if (e.hash == hash && e.key.equals(key)) {
                if (prev != null) {
                    prev.next = e.next;
                } else {
                    tab[index] = e.next;
                }

                this.count--;
                V oldValue = e.value;
                e.value = null;
                return oldValue;
            }

            prev = e;
        }

        return null;
    }

    private static class Entry<K,V>{
        final int hash;
        final K key;
        V value;
        Entry<K,V> next;

        protected Entry(int hash, K key, V value, Entry<K,V> next) {
            this.hash = hash;
            this.key =  key;
            this.value = value;
            this.next = next;
        }
    }
}
